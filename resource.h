//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DesktopTunes.rc
//
#define IDR_MAINFRAME                   101
#define IDD_DESKTOPTUNES_DIALOG         102
#define IDR_TRAY_MENU                   130
#define IDR_JS_APP                      131
#define IDR_HTML_INDEX                  132
#define IDR_CSS_STYLE                   133
#define ID_DESKTOPTUNES_CLOSE           32771
#define ID_DESKTOPTUNES_AUDIODEVICES    32772
#define ID_DESKTOPTUNES_DESKTOPTUNES    32773

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
