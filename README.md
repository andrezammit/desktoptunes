# DesktopTunes #

Displays information on the currently playing song in iTunes along with a sound analyzer at the bottom of the desktop.

![DesktopTunes Screenshot](https://i.imgur.com/OdJ2o3k.png "Screenshot")