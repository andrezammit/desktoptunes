#include "stdafx.h"

#include "DesktopTunes.h"
#include "ITunesInterface.h"

CITunesInterface::CITunesInterface()
{
	m_isInitialized = false;
}

CITunesInterface::~CITunesInterface()
{
}

bool CITunesInterface::CheckIfiTunesIsRunning()
{
	if (::FindWindow(_T("iTunes"), _T("iTunes")) == NULL)
	{
		if (m_pITunes != nullptr)
		{
			m_pITunes.Release();
			m_pITunes = nullptr;
		}

		return false;
	}
	
	if (m_pITunes == nullptr)
	{
		m_pITunes.CoCreateInstance(CLSID_iTunesApp, nullptr, CLSCTX_LOCAL_SERVER);
		m_isInitialized = true;
	}

	return true;
}

void CITunesInterface::CheckCurrentTrack()
{
	if (!CheckIfiTunesIsRunning())
	{
		if (m_isInitialized)
		{
			theApp.ITunesClosedNotif();
			m_isInitialized = false;
		}

		return;
	}

	CComPtr<IITTrack> pCurrentTrack;
	
	if (FAILED(m_pITunes->get_CurrentTrack(&pCurrentTrack)))
	{
		return;
	}

	if (pCurrentTrack == nullptr)
	{
		return;
	}

	long oldTrackId = 0;

	if (m_pCurrentTrack != nullptr)
	{
		m_pCurrentTrack->get_TrackID(&oldTrackId);
	}

	long newTrackId = 0;

	if (FAILED(pCurrentTrack->get_TrackID(&newTrackId)))
	{
		return;
	}

	if (oldTrackId == newTrackId)
	{
		long playerPos = 0;
		if (FAILED(m_pITunes->get_PlayerPosition(&playerPos)))
		{
			return;
		}

		theApp.UpdateCurrentTime(playerPos);
		return;
	}

	m_pCurrentTrack = pCurrentTrack;

	CComBSTR bstrTrack;
	if (FAILED(pCurrentTrack->get_Name(&bstrTrack)))
	{
		return;
	}

	CComBSTR bstrArtist;
	if (FAILED(pCurrentTrack->get_Artist(&bstrArtist)))
	{
		return;
	}

	CComBSTR bstrAlbum;

	if (FAILED(pCurrentTrack->get_Album(&bstrAlbum)))
	{
		return;
	}

	long duration = 0;
	if (FAILED(pCurrentTrack->get_Duration(&duration)))
	{
		return;
	}

	CCurrentTrack currentTrack;

	currentTrack.m_track = bstrTrack;
	currentTrack.m_artist = bstrArtist;
	currentTrack.m_album = bstrAlbum;
	currentTrack.m_duration = duration;

	CComPtr<IITArtworkCollection> pArtworkCollection;
	
	if (FAILED(pCurrentTrack->get_Artwork(&pArtworkCollection)))
	{
		return;
	}

	LONG count = 0;
	
	if (FAILED(pArtworkCollection->get_Count(&count)))
	{
		return;
	}

	if (count > 0)
	{
		CComPtr<IITArtwork> pArtwork;
		
		if (FAILED(pArtworkCollection->get_Item(1, &pArtwork)))
		{
			return;
		}

		CString rArtworkPath;
		rArtworkPath.Format(_T("%s\\artwork.jpg"), theApp.GetTempFolder());

		pArtwork->SaveArtworkToFile(CComBSTR(rArtworkPath));
	}

	theApp.UpdateCurrentTrack(currentTrack);
}