#pragma once

#include "EventHandler.h"

class CBrowserInterface
{
public:
	CBrowserInterface();
	~CBrowserInterface();

	void PrepareToClose();
	
	void UpdateSize(CRect& rect);
	void ShowMusicInfo(bool show);
	void SetMousePosition(int x, int y);
	void UpdateCurrentTime(long currentTime);
	void UpdateCurrentTrack(CCurrentTrack& currentTrack);
	void SetAudioDevice(const CString& deviceId, const CString& deviceName);

	bool IsReadyToClose();

	bool ExecuteJavaScript(const CString& jsCode);
	bool Create(HWND hWnd, CRect& rect, const CString& lastDeviceId);

	static CString EscapeString(const CString& string);

private:
	CefRefPtr<CEventHandler> m_pEventHandler;
};

