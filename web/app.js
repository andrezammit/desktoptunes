var _barWidth = 15;

var _audioCtx = new window.AudioContext();
var analyser = null;

var isMusicInfoVisible = false;

var currentSongLength = 0;

var currentAudioDeviceId = "";
var currentAudioDeviceName = "";

var UI = {};

function enumerateDevices(callback) {
    function displayDevices(devices) {
        console.log('DEVENUM:\2');

        devices.forEach(
            function (device) {
                if (device.label === "") {
                    return;
                }

                if (currentAudioDeviceName === "") {
                    currentAudioDeviceName = device.label;
                }

                var msg = 'DEVENUM:\1' + device.kind + "\1" + device.label + "\1" + device.deviceId;

                if (currentAudioDeviceName === device.label) {
                    currentAudioDeviceId = device.deviceId;
                    msg += "\1" + "1";
                }

                console.log(msg);
            });

        if (callback !== undefined) {
            callback();
        }
    }

    navigator.mediaDevices.enumerateDevices()
        .then(displayDevices)
        .catch(function (err) {
            console.log(err.name + ": " + err.message);
        });
}

function errorCallback(error) {
    console.log('Failed to get media stream source. ' + error);
}

function setupAudioStream(deviceId, deviceName) {
    var constraints = {
        audio: {
            'optional': [{ sourceId: deviceId }]
        }
    };

    navigator.getUserMedia(constraints,
        function (localMediaStream) {
            var audioSource = _audioCtx.createMediaStreamSource(localMediaStream);
            
            currentAudioDeviceId = deviceId;
            currentAudioDeviceName = deviceName;

            onAudioReady(audioSource);
            enumerateDevices();

        }, errorCallback);
}

function cacheUIElements() {
    UI.musicInfo = document.querySelector('#musicInfo');
    UI.artist = document.querySelector('#artist');
    UI.track = document.querySelector('#track');
    UI.album = document.querySelector('#album');
    UI.duration = document.querySelector('#duration');
    UI.artworkImg = document.querySelector('#artwork > img');
    UI.currentTime = document.querySelector('#currentTime');
    UI.partialBar = document.querySelector('#partialBar');
    UI.canvas = document.querySelector('#analyzerCanvas');

    UI.canvasCtx = UI.canvas.getContext('2d');
}

function setCurrentAudioDeviceName() {
    var searchParams = (new URL(document.location)).searchParams;
    var lastDeviceName = searchParams.get("lastDeviceName");

    if (lastDeviceName === null) {
        return;
    }

    console.log("Setting default device name to: " + lastDeviceName);
    currentAudioDeviceName = lastDeviceName;
}

window.onload = function () {
    console.log("Window loaded");

    cacheUIElements();

    setCurrentAudioDeviceName();
    
    showMusicInfo(false);

    resizeCanvas();
    
    //setupAudioStream('de6925c13e6dd993dcc06980742565e9907e87bb1744d59511b7a51d180e0547');

    enumerateDevices(onInitialDeviceEnum);
};

function onInitialDeviceEnum() {
    setupAudioStream(currentAudioDeviceId, currentAudioDeviceName);
}

window.onresize = function () {
    resizeCanvas();
    showMusicInfo(isMusicInfoVisible);
};

function resizeCanvas() {
    var canvasParent = document.querySelector('#canvasParent');
    UI.canvas.width = canvasParent.clientWidth;

    updateFftSize();
}

function onAudioReady(audioSource) {
    var deviceName = currentAudioDeviceName;
    console.log("Starting draw function for device " + deviceName);
    
    analyser = _audioCtx.createAnalyser();
    audioSource.connect(analyser);

    updateFftSize();

    function draw() {
        if (deviceName !== currentAudioDeviceName) {
            console.log("Closing draw function for device " + deviceName);
            return;
        }

        var bufferLength = analyser.frequencyBinCount;
        var dataArray = new Uint8Array(bufferLength);

        drawVisual = requestAnimationFrame(draw);

        analyser.getByteFrequencyData(dataArray);

        UI.canvasCtx.clearRect(0, 0, UI.canvas.width, UI.canvas.height);

        var visibleBuffLen = Math.floor(0.9 * bufferLength);
        var tmpBarWidth = UI.canvas.width / visibleBuffLen;

        var barOffset = 0;
        for (var i = 0; i < visibleBuffLen; i++) {
            var barHeight = dataArray[i] / 2;

            UI.canvasCtx.fillStyle = "rgba(" + Math.floor(100 + barHeight) + ", 50, 50, 0.7)";
            UI.canvasCtx.fillRect(barOffset, UI.canvas.height - barHeight / 2, tmpBarWidth, barHeight);

            barOffset += tmpBarWidth + 1;
        }
    }

    requestAnimationFrame(draw);
}

function nearestPow2(size) {
    return Math.pow(2, Math.round(Math.log(size) / Math.log(2)));
}

function secondsToTime(seconds) {
    var minutes = Math.floor(seconds / 60);
    var secondsLeft = seconds - (minutes * 60);

    if (secondsLeft < 10) {
        secondsLeft = "0" + secondsLeft;
    }

    return minutes + ":" + secondsLeft;
}

function updateFftSize() {
    if (analyser === null) {
        return;
    }

    var fftSize = (UI.canvas.width / _barWidth) * 2;
    var tmpSize = nearestPow2(fftSize);

    if (tmpSize < fftSize) {
        var pow = Math.log(tmpSize) / Math.log(2);
        fftSize = Math.pow(2, pow + 1);
    }

    analyser.fftSize = Math.max(32, tmpSize);
}

function getRandomNumber()
{
    return Math.floor((Math.random() * 100) + 1);
}

function updateTrack(artist, song, album, duration) {
    UI.artist.textContent = artist;
    UI.track.textContent = song;
    UI.album.textContent = album;
    UI.duration.textContent = secondsToTime(duration);

    var filename = "artwork.jpg?" + getRandomNumber();
    UI.artworkImg.setAttribute("src", filename);

    currentSongLength = duration;
}

function getCurrentTimePercentage(currentTime) {
    return (currentTime / currentSongLength) * 100;
}

function updateCurrentTime(currentTime) {
    UI.currentTime.textContent = secondsToTime(currentTime);
    UI.partialBar.style.width = getCurrentTimePercentage(currentTime) + "%";
}

function showMusicInfo(show) {
    isMusicInfoVisible = show;

    var style = window.getComputedStyle(UI.musicInfo);
    var margin = parseInt(style.getPropertyValue('margin-right'));

    var right = show === true ? margin + "px" : (-(UI.musicInfo.offsetWidth) - margin) + "px";
    UI.musicInfo.style.right = right;
}
