#pragma once

#include "itunes sdk\iTunesCOMInterface.h"

class CITunesInterface
{
public:
	CITunesInterface();
	~CITunesInterface();

	void CheckCurrentTrack();

private:
	bool m_isInitialized;

	CComPtr<IiTunes> m_pITunes;
	CComPtr<IITTrack> m_pCurrentTrack;

	bool CheckIfiTunesIsRunning();
};

