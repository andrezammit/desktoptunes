
// DesktopTunes.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"

#include "DesktopTunes.h"
#include "DesktopTunesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

CAudioDevice::CAudioDevice()
{
	m_isInUse = false;
}

CAudioDevice::CAudioDevice(const CString& audioDeviceString)
{
	vector<CString> deviceProperties;

	int start = 0;
	CString token = audioDeviceString.Tokenize(_T("\1"), start);

	while (!token.IsEmpty())
	{
		deviceProperties.push_back(token);
		token = audioDeviceString.Tokenize(_T("\1"), start);
	}

	if (deviceProperties.size() < 4)
	{
		deviceProperties.resize(4);
	}

	m_deviceId = deviceProperties[2];
	m_deviceName = deviceProperties[1];

	m_isInUse = deviceProperties[3] == _T("1");
}

// CDesktopTunesApp

BEGIN_MESSAGE_MAP(CDesktopTunesApp, CWinApp)
END_MESSAGE_MAP()

// CDesktopTunesApp construction

CDesktopTunesApp::CDesktopTunesApp()
{
}

CDesktopTunesApp::~CDesktopTunesApp()
{
}

// The one and only CDesktopTunesApp object

CDesktopTunesApp theApp;

// CDesktopTunesApp initialization

BOOL CDesktopTunesApp::InitInstance()
{
	CoInitialize(NULL);

	if (!CreateTempFolder())
	{
		return FALSE;
	}

	if (!ExtractResources())
	{
		return FALSE;
	}

	InitSettings();
	LoadSettings();

	if (!InitBrowser())
	{
		return FALSE;
	}

	CWinApp::InitInstance();

	CDesktopTunesDlg dlg(CWnd::FromHandle(GetDesktopWindow()));
	m_pMainWnd = &dlg;
	
	dlg.DoModal();

	DeleteTempFolder();

	return FALSE;
}

int CDesktopTunesApp::ExitInstance()
{
	CefShutdown();
	CoUninitialize();

	return CWinApp::ExitInstance();
}

void CDesktopTunesApp::InitSettings()
{
	free((void*)m_pszProfileName);

	TCHAR* pLocalAppDataPath = nullptr;
	SHGetKnownFolderPath(FOLDERID_LocalAppData, NULL, NULL, &pLocalAppDataPath);

	CString localAppDataPath = pLocalAppDataPath;
	CoTaskMemFree(pLocalAppDataPath);

	CString settingsPath = localAppDataPath;
	settingsPath.Append(_T("\\DesktopTunes"));

	CreateDirectory(settingsPath, NULL);

	settingsPath.Append(_T("\\settings.ini"));

	m_pszProfileName = _tcsdup(settingsPath);
}

void CDesktopTunesApp::LoadSettings()
{
	m_settings.m_lastDeviceName = GetProfileString(_T("Devices"), _T("LastDeviceName"));
}

bool CDesktopTunesApp::InitBrowser()
{
	CefMainArgs mainargs(m_hInstance);

	m_cefApp = new CBrowserApp();
	CefExecuteProcess(mainargs, m_cefApp, nullptr);

	CefSettings settings;
	settings.multi_threaded_message_loop = true;

#ifdef _DEBUG
	settings.single_process = true;
#endif 

	CString logFilePath;
	logFilePath.Format(_T("%s\\debug.log"), GetTempFolder());

	cef_string_set(logFilePath, logFilePath.GetLength(), &settings.log_file, true);

	if (!CefInitialize(mainargs, settings, m_cefApp, nullptr))
	{
		return false;
	}

	return true;
}

bool CDesktopTunesApp::CreateTempFolder()
{
	CString tempFolder;
	
	GetTempPath(MAX_PATH, tempFolder.GetBuffer(MAX_PATH + 1));
	tempFolder.ReleaseBuffer();

	if (tempFolder.IsEmpty())
	{
		return false;
	}

	tempFolder.Append(_T("DesktopTunes"));

	if (!CreateDirectory(tempFolder, NULL))
	{
		if (GetLastError() != ERROR_ALREADY_EXISTS)
		{
			return false;
		}
	}

	m_tempFolder = tempFolder;
	return true;
}

CString CDesktopTunesApp::GetConsoleLogPath()
{
	if (!m_consoleLogPath.IsEmpty())
	{
		return m_consoleLogPath;
	}

	m_consoleLogPath.Format(_T("%s\\console.log"), GetTempFolder());
	return m_consoleLogPath;
}

CString CDesktopTunesApp::GetModuleFolder()
{
	if (!m_moduleFolder.IsEmpty())
	{
		return m_moduleFolder;
	}

	CString moduleFilename;

	GetModuleFileName(NULL, moduleFilename.GetBuffer(MAX_PATH + 1), MAX_PATH);
	moduleFilename.ReleaseBuffer();
	
	int pos = moduleFilename.ReverseFind(_T('\\'));

	if (pos == -1)
	{
		ASSERT(FALSE);
		return _T("");
	}

	m_moduleFolder = moduleFilename.Left(pos);
	return m_moduleFolder;
}

bool CDesktopTunesApp::CopyFileToTemp(const CString& path)
{
	CString source;
	source.Format(_T("%s\\%s"), GetModuleFolder(), path);

	int pos = path.ReverseFind(_T('\\'));

	CString filename;

	if (pos != -1)
	{
		filename = path.Mid(pos + 1);
	}
	else
	{
		filename = path;
	}

	CString destination;
	destination.Format(_T("%s\\%s"), m_tempFolder, filename);

	if (!CopyFile(source, destination, FALSE))
	{
		return false;
	}

	return true;
}

bool CDesktopTunesApp::DeleteTempFolder()
{
	TCHAR path[MAX_PATH + 1] = { 0 };
	
	_tcscpy_s(path, m_tempFolder);
	path[m_tempFolder.GetLength() + 1] = 0;

	SHFILEOPSTRUCTW shFileOp = { 0 };

	shFileOp.pFrom = path;
	shFileOp.wFunc = FO_DELETE;
	shFileOp.fFlags = FOF_NO_UI;

	if (SHFileOperation(&shFileOp) != ERROR_SUCCESS)
	{
		return false;
	}

	return true;
}

void CDesktopTunesApp::UpdateCurrentTrack(CCurrentTrack& currentTrack)
{
	m_pMainWnd->SendMessage(WM_UPDATE_CURRENT_TRACK, NULL, (LPARAM)&currentTrack);
}

void CDesktopTunesApp::UpdateCurrentTime(long currentTime)
{
	m_pMainWnd->SendMessage(WM_UPDATE_CURRENT_TIME, NULL, (LPARAM)&currentTime);
}

BOOL CDesktopTunesApp::PumpMessage()
{
	auto result = CWinApp::PumpMessage();
	return result;
}

void CDesktopTunesApp::ITunesClosedNotif()
{
	m_pMainWnd->SendMessage(WM_ITUNES_CLOSED_NOTIF);
}

void CDesktopTunesApp::AudioDeviceNotif(unique_ptr<CString>& pMessage)
{
	if (m_pMainWnd->PostMessage(WM_AUDIO_DEVICE_NOTIF, NULL, (LPARAM)pMessage.get()))
	{
		pMessage.release();
	}
}

void CDesktopTunesApp::WindowLoadedNotif()
{
	m_pMainWnd->SendMessage(WM_WINDOW_LOADED_NOTIF);
}

bool CDesktopTunesApp::ExtractResources()
{
	map<UINT, CString> resourcesToExtract;

	resourcesToExtract[IDR_JS_APP] = _T("app.js");
	resourcesToExtract[IDR_CSS_STYLE] = _T("style.css");
	resourcesToExtract[IDR_HTML_INDEX] = _T("index.html");

	for (auto resource : resourcesToExtract)
	{
		if (!ExtractResource(resource.first, resource.second))
		{
			return false;
		}
	}

	return true;
}

bool CDesktopTunesApp::ExtractResource(UINT uID, const CString& filename)
{
	HRSRC resource = FindResource(NULL, MAKEINTRESOURCE(uID), RT_RCDATA);
	
	if (resource == NULL)
	{
		return false;
	}

	DWORD resourceSize = SizeofResource(NULL, resource);
	HGLOBAL resourceData = LoadResource(NULL, resource);

	if (resourceData == NULL)
	{
		return false;
	}

	void* binaryData = ::LockResource(resourceData);

	if (binaryData == nullptr)
	{
		UnlockResource(resourceData);
		return false;
	}

	CString path;
	path.Format(_T("%s\\%s"), m_tempFolder, filename);

	CFile file;
	if (!file.Open(path, CFile::modeCreate | CFile::modeReadWrite))
	{
		UnlockResource(resourceData);
		return false;
	}

	file.Write(binaryData, resourceSize);
	UnlockResource(resourceData);

	return true;
}