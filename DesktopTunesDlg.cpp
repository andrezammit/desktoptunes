// DesktopTunesDlg.cpp : implementation file
//

#include "stdafx.h"

#include "DesktopTunes.h"
#include "DesktopTunesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TRAY_ICON_ID			1001
#define TIMER_SONG_UPDATE		1001
#define SONG_UPDATE_TIMEOUT		500

// CDesktopTunesDlg dialog

CDesktopTunesDlg::CDesktopTunesDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_DESKTOPTUNES_DIALOG, pParent)
{
	m_isTopMost = false;
	m_isMusicInfoVisible = false;

	m_shellTrayWnd = NULL;
	m_cachedForegroundWnd = NULL;
}

CDesktopTunesDlg::~CDesktopTunesDlg()
{
}

void CDesktopTunesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CDesktopTunesDlg, CDialogEx)
	ON_WM_SIZE()
	ON_WM_TIMER()
	ON_WM_CLOSE()

	ON_COMMAND(ID_DESKTOPTUNES_CLOSE, OnTrayClose)

	ON_COMMAND_RANGE(WM_DEVICE_START, WM_DEVICE_END, OnTrayDevice)

	ON_MESSAGE(WM_TRAY_NOTIF, OnTrayNotif)
	ON_MESSAGE(WM_AUDIO_DEVICE_NOTIF, OnAudioDeviceNotif)
	ON_MESSAGE(WM_ITUNES_CLOSED_NOTIF, OnITunesClosedNotif)
	ON_MESSAGE(WM_WINDOW_LOADED_NOTIF, OnWindowLoadedNotif)
	ON_MESSAGE(WM_UPDATE_CURRENT_TIME, OnUpdateCurrentTime)
	ON_MESSAGE(WM_UPDATE_CURRENT_TRACK, OnUpdateCurrentTrack)
END_MESSAGE_MAP()

// CDesktopTunesDlg message handlers

BOOL CDesktopTunesDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	CreateTrayIcon();

	SetLayeredWindowAttributes(RGB(1, 1, 1), 200, LWA_ALPHA | LWA_COLORKEY);

	ResizeWindow();

	CRect rect;
	GetClientRect(&rect);

	m_browserInterface.Create(GetSafeHwnd(), rect, theApp.m_settings.m_lastDeviceName);

	m_shellTrayWnd = ::FindWindow(_T("Shell_TrayWnd"), NULL);

	return TRUE;
}

void CDesktopTunesDlg::ResizeWindow()
{
	CWnd* pWnd = GetDesktopWindow();

	CRect rect;
	pWnd->GetWindowRect(rect);

	rect.bottom -= 1;

	MoveWindow(rect);
}

void CDesktopTunesDlg::OnClose()
{
	if (!m_browserInterface.IsReadyToClose())
	{
		m_browserInterface.PrepareToClose();
		return;
	}

	RemoveTrayIcon();

	CDialogEx::OnClose();
}

void CDesktopTunesDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CRect rect;
	GetClientRect(&rect);

	m_browserInterface.UpdateSize(rect);
}

void CDesktopTunesDlg::OnTimer(UINT_PTR uIDEvent)
{
	POINT pt = { 0 };
	GetCursorPos(&pt);

	m_browserInterface.SetMousePosition(pt.x, pt.y);

	ForegroundWindowCheck();
	m_itunesInterface.CheckCurrentTrack();
}

void CDesktopTunesDlg::ForegroundWindowCheck()
{
	HWND foregroundWnd = ::GetForegroundWindow();

	if (foregroundWnd == GetSafeHwnd())
	{
		return;
	}

	if (m_cachedForegroundWnd == foregroundWnd)
	{
		return;
	}

	m_cachedForegroundWnd = foregroundWnd;

	HWND insertAfterWnd = NULL;

	if (foregroundWnd == m_shellTrayWnd)
	{
		insertAfterWnd = HWND_NOTOPMOST;
	}
	else
	{
		CString rClassName;

		GetClassName(foregroundWnd, rClassName.GetBuffer(512), 512);
		rClassName.ReleaseBuffer();

		if (rClassName == _T("WorkerW") &&
			::GetParent(foregroundWnd) == NULL)
		{
			m_isTopMost = true;
			insertAfterWnd = m_shellTrayWnd;
		}
		else 
		{
			m_isTopMost = false;
			insertAfterWnd = HWND_BOTTOM;
		}
	}

	CRect rect;
	GetClientRect(&rect);

	::SetWindowPos(GetSafeHwnd(), insertAfterWnd, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_NOACTIVATE);
}

LRESULT CDesktopTunesDlg::OnUpdateCurrentTrack(WPARAM wParam, LPARAM lParam)
{
	if (!m_isMusicInfoVisible)
	{
		m_browserInterface.ShowMusicInfo(true);
		m_isMusicInfoVisible = true;
	}

	CCurrentTrack* pCurrentTrack = (CCurrentTrack*) lParam;
	m_browserInterface.UpdateCurrentTrack(*pCurrentTrack);

	return 0;
}

LRESULT CDesktopTunesDlg::OnUpdateCurrentTime(WPARAM wParam, LPARAM lParam)
{
	long* pCurrentTime = (long*)lParam;
	m_browserInterface.UpdateCurrentTime(*pCurrentTime);

	return 0;
}

LRESULT CDesktopTunesDlg::OnITunesClosedNotif(WPARAM wParam, LPARAM lParam)
{
	m_browserInterface.ShowMusicInfo(false);
	m_isMusicInfoVisible = false;

	return 0;
}

LRESULT CDesktopTunesDlg::OnWindowLoadedNotif(WPARAM wParam, LPARAM lParam)
{
	SetTimer(TIMER_SONG_UPDATE, SONG_UPDATE_TIMEOUT, NULL);
	return 0;
}

LRESULT CDesktopTunesDlg::OnAudioDeviceNotif(WPARAM wParam, LPARAM lParam)
{
	unique_ptr<CString> pMessage((CString*) lParam);
	ParseAudioDeviceNotif(*pMessage);

	return 0;
}

void CDesktopTunesDlg::ParseAudioDeviceNotif(const CString& audioDeviceString)
{
	if (audioDeviceString == _T("\2"))
	{
		m_audioDevices.clear();
		return;
	}

	CAudioDevice audioDevice(audioDeviceString);
	m_audioDevices.push_back(audioDevice);
}

void CDesktopTunesDlg::CreateTrayIcon()
{
	NOTIFYICONDATA notifIconData = { 0 };

	notifIconData.uID = TRAY_ICON_ID;
	notifIconData.hWnd = GetSafeHwnd();
	notifIconData.cbSize = sizeof(notifIconData);
	notifIconData.uCallbackMessage = WM_TRAY_NOTIF;
	notifIconData.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	notifIconData.hIcon = LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME));

	_tcscpy_s(notifIconData.szTip, _T("DesktopTunes"));

	Shell_NotifyIcon(NIM_ADD, &notifIconData);
}

void CDesktopTunesDlg::RemoveTrayIcon()
{
	NOTIFYICONDATA notifIconData = { 0 };

	notifIconData.uID = TRAY_ICON_ID;
	notifIconData.cbSize = sizeof(notifIconData);

	Shell_NotifyIcon(NIM_DELETE, &notifIconData);
}

LRESULT CDesktopTunesDlg::OnTrayNotif(WPARAM wParam, LPARAM lParam)
{
	switch (LOWORD(lParam))
	{
	case WM_RBUTTONDOWN:
	case WM_CONTEXTMENU:
		{
			CPoint pt;
			GetCursorPos(&pt);

			ShowTrayContextMenu(pt);
		}
		break;
	}
	
	return 0;
}

void CDesktopTunesDlg::OnTrayDevice(UINT command)
{
	auto deviceIndex = command - (WM_DEVICE_START);
	auto audioDevice = m_audioDevices[deviceIndex];

	m_browserInterface.SetAudioDevice(audioDevice.m_deviceId, audioDevice.m_deviceName);

	theApp.WriteProfileString(_T("Devices"), _T("LastDeviceName"), audioDevice.m_deviceName);
}

void CDesktopTunesDlg::ShowTrayContextMenu(const CPoint& pt)
{
	CMenu trayMenu;
	trayMenu.LoadMenu(IDR_TRAY_MENU);
	
	CMenu devicesMenu;
	devicesMenu.CreatePopupMenu();

	for (int cnt = 0; cnt < m_audioDevices.size(); cnt++)
	{
		CAudioDevice& audioDevice = m_audioDevices[cnt];

		auto flags = MF_STRING;

		if (audioDevice.m_isInUse)
		{
			flags |= MF_CHECKED;
		}

		UINT_PTR menuItemID = WM_DEVICE_START + cnt;
		devicesMenu.AppendMenu(flags, menuItemID, audioDevice.m_deviceName);
	}

	CMenu* pSubMenu = trayMenu.GetSubMenu(0);
	pSubMenu->InsertMenuW(2, MF_BYPOSITION | MF_POPUP, (UINT_PTR)devicesMenu.m_hMenu, _T("&Audio Devices"));

	SetForegroundWindow();
	pSubMenu->TrackPopupMenu(TPM_BOTTOMALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON, pt.x, pt.y, this);

	PostMessage(WM_NULL);
}

void CDesktopTunesDlg::OnTrayClose()
{
	KillTimer(TIMER_SONG_UPDATE);
	m_browserInterface.PrepareToClose();
}
