
// DesktopTunes.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "EventHandler.h"

#define WM_UPDATE_CURRENT_TRACK		WM_USER + 100
#define WM_UPDATE_CURRENT_TIME		WM_USER + 101
#define WM_TRAY_NOTIF				WM_USER + 102
#define WM_ITUNES_CLOSED_NOTIF		WM_USER + 103
#define WM_AUDIO_DEVICE_NOTIF		WM_USER + 104
#define WM_WINDOW_LOADED_NOTIF		WM_USER + 105

#define WM_DEVICE_START				WM_USER + 200
#define WM_DEVICE_END				WM_USER + 299

class CCurrentTrack
{
public:
	CCurrentTrack()
	{
		m_duration = 0;
	}

	long m_duration;

	CString m_track;
	CString m_artist;
	CString m_album;
};

class CAudioDevice
{
public:
	CAudioDevice();
	CAudioDevice(const CString& audioDeviceString);

	bool m_isInUse;

	CString m_deviceId;
	CString m_deviceName;
};

class CSettings
{
public:
	CSettings()
	{
	}

	CString m_lastDeviceName;
};

class CDesktopTunesApp : public CWinApp
{
public:
	CDesktopTunesApp();
	~CDesktopTunesApp();

	CSettings m_settings;

	void ITunesClosedNotif();
	void WindowLoadedNotif();
	void UpdateCurrentTime(long currentTime);
	void UpdateCurrentTrack(CCurrentTrack& currentTrack);
	void AudioDeviceNotif(unique_ptr<CString>& pMessage);

	CString GetConsoleLogPath();

	CString GetTempFolder() { return m_tempFolder; }

protected:
	virtual BOOL InitInstance();
	
	int ExitInstance();

	DECLARE_MESSAGE_MAP()

private:
	CString m_tempFolder;
	CString m_moduleFolder;

	CString m_consoleLogPath;

	CefRefPtr<CBrowserApp> m_cefApp;

	void InitSettings();
	void LoadSettings();

	bool InitBrowser();
	bool CreateTempFolder();
	bool DeleteTempFolder();
	bool ExtractResources();

	bool CopyFileToTemp(const CString& path);
	bool ExtractResource(UINT uID, const CString& filename);

	BOOL PumpMessage();

	CString GetModuleFolder();
};

extern CDesktopTunesApp theApp;
