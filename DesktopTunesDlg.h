// DesktopTunesDlg.h : header file
//

#pragma once

#include "ITunesInterface.h"
#include "BrowserInterface.h"

class CDesktopTunesDlg : public CDialogEx
{
public:
	CDesktopTunesDlg(CWnd* pParent = NULL);
	~CDesktopTunesDlg();

#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DESKTOPTUNES_DIALOG };
#endif

protected:
	virtual void OnClose();
	
	virtual void OnTimer(UINT_PTR uIDEvent);
	virtual void OnSize(UINT nType, int cx, int cy);
	virtual void DoDataExchange(CDataExchange* pDX);

	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()

private:
	bool m_isTopMost;
	bool m_isMusicInfoVisible;

	HWND m_shellTrayWnd;
	HWND m_cachedForegroundWnd;

	CITunesInterface m_itunesInterface;
	CBrowserInterface m_browserInterface;

	vector<CAudioDevice> m_audioDevices;

	void ResizeWindow();
	void CreateTrayIcon();
	void RemoveTrayIcon();
	void ForegroundWindowCheck();

	void OnTrayClose();
	void OnTrayDevice(UINT command);

	void ShowTrayContextMenu(const CPoint& pt);
	void ParseAudioDeviceNotif(const CString& audioDevice);

	LRESULT OnTrayNotif(WPARAM wParam, LPARAM lParam);
	LRESULT OnAudioDeviceNotif(WPARAM wParam, LPARAM lParam);
	LRESULT OnUpdateCurrentTime(WPARAM wParam, LPARAM lParam);
	LRESULT OnITunesClosedNotif(WPARAM wParam, LPARAM lParam);
	LRESULT OnWindowLoadedNotif(WPARAM wParam, LPARAM lParam);
	LRESULT OnUpdateCurrentTrack(WPARAM wParam, LPARAM lParam);
};
